def suma(n1: int, n2: int, n3: int, n4: int) -> int:
    return n1+n2+n3+n4

def resta(n1: int, n2: int) -> int:
    if n1 > n2:
        return n1 - n2
    else:
        return n2 - n1

print("1+2+3+4 = " + str(suma(1,2,3,4)))

print("6-5 = " + str(resta(5,6)))

print("8-7 = " + str(resta(7,8)))